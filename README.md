//Market me up is a market place for all farmer. Basically large scale farmers.

//Follow the steps below to setupMarket me up.

Open Source Ecommerce Open Source Framework for Everyone built with Sails.js (Node.js + MongoDB)

System Dependencies
--------

* MongoDB
* Node.js
* npm v

How To Run
--------

- With Github

git clone https://gitlab.com/josephdilas/marketmeup.git

npm install

node app.js

- CONFIGURATION

     [1] Configure and run a mongodb instance ( the default is  host: 'localhost', port: 27017)

     [2] Create a mongoDb database with name "Databasename"

     [3] Default login credential : admin@admin.com / admin

     [4] Go to http://localhost:1338/install


- FRONT END

     Navigate to http://localhost:1338
     You can loggin with the user:  admin@admin.com and password: admin

- BACK END

     You can access at http://localhost:1338/admin

     You can loggin with the user:  admin@admin.com and password: admin
